# task1

**Выполнил Никифоров Иван БСБО-06-19**

## Ввод данных

Ввод данных осуществляется через файл .gitlab-ci.yml, в stage deploy при создании файла file.txt


## Вывод данных

Ввод данных осуществляется через artefacts в файл result.txt

## Выполнение

Парсим курс доллара, получаем от пользователя булевый тип данных (false - перевод из рубля в доллар, true - из доллара в рубли), а также количество валюты

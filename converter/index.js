var fs = require('fs');

getCurrencies();
let array = fs.readFileSync("file.txt", "utf8").split('\\n');
let myValue =(array[0]);
let check = (myValue.toLowerCase() === 'true');
let a = Number(array[1]);
console.log(check);
console.log(array);
rates ={};
async function getCurrencies() {
    let response = await fetch('https://www.cbr-xml-daily.ru/daily_json.js');
    if (response.ok) {
        var json = await response.json();
        rates.USD = parseFloat(json.Valute.USD.Value);
    } else {
        alert("Ошибка HTTP: " + response.status);
    }   
}

function calc(){
    let converted = 0;
    console.log(rates.USD+'rub in usd')
    if (check){
        console.log('Convert usd to rub:')
        converted= (a*rates.USD).toFixed(2);
    }else{
        console.log('Convert rub to usd:')
        converted= (a/rates.USD).toFixed(2);
    }
    const add_text = check ? 'рублей' : 'долларов';
    const text = `Переведенная сумма составила ${converted} ${add_text}`;

    setTimeout(() => { console.log(text); }, 8000);

    setTimeout(() => { fs.writeFileSync('result.txt',text); },5000);
}
setTimeout(calc,8000)
